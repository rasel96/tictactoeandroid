package com.rasel96.tictactoe;

import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button[][] buttons = new Button[3][3];      // 3x3 Matrix of Button
    private TextView textViewP1, textViewP2;            // Players Text Views
    private Button  bReset;                             // Reset Button
    private int roundCounts;                            // Counting Rounds to 9
    private int p1Points, p2Points;                     // Score
    private boolean p1turn = true;                      // Shifting turns

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textViewP1 = findViewById(R.id.textViewP1);
        textViewP2 = findViewById(R.id.textViewP2);
        bReset = findViewById(R.id.buttonReset);

        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                String buttonID = "b" + i + j;
                int resID = getResources().getIdentifier(buttonID, "id", getPackageName());
                buttons[i][j] = findViewById(resID);
                buttons[i][j].setOnClickListener(this);
            }
        }

        bReset.setOnClickListener(new View.OnClickListener() {      // Reset button doesn't share
            @Override                                               // the same Listener.
            public void onClick(View v) {
                if(gameIsEnable()){
                    resetGame(0,0);
                    resetBoard();
                }else{
                    resetGame(p1Points, p2Points);
                    bReset.setText(R.string.reset);
                    resetBoard();
                    enableGame(true);
                }
            }
        });

    }

    @Override
    public void onClick(View v) {

        if(!((Button) v).getText().toString().equals("")){
            return;
        }
        if(p1turn){
            ((Button) v).setText("X");
        }else{
            ((Button) v).setText("O");
        }
        roundCounts++;
        if(checkForWin()){
            if(p1turn){
                p1Wins();
            }else{
                p2Wins();
            }
        }else if(roundCounts == 9){
            draw();
        }else{
            p1turn = !p1turn;
        }

    }

    private boolean checkForWin(){
        String[][] field = new String[3][3];
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                field[i][j] = buttons[i][j].getText().toString();
            }
        }

        for(int i = 0; i < 3; i++){                     // Checking the ROWs
            if(field[i][0].equals(field[i][1])
                    && field[i][0].equals(field[i][2])
                    && !field[i][0].equals("")){
                return true;
            }
        }

        for(int i = 0; i < 3; i++){                     // Checking the COLUMNs
            if(field[0][i].equals(field[1][i])
                    && field[0][i].equals(field[2][i])
                    && !field[0][i].equals("")){
                return true;
            }
        }

        if(field[0][0].equals(field[1][1])              // Diagonal Left
                && field[0][0].equals(field[2][2])
                && !field[0][0].equals("")){
            return true;
        }

        if(field[2][0].equals(field[1][1])              // Diagonal Right
                && field[2][0].equals(field[0][2])
                && !field[2][0].equals("")){
            return true;
        }
        return false;           // No one won yet!
    }

    private void p1Wins(){
        p1Points++;
        Toast.makeText(this, "Player 1 wins!", Toast.LENGTH_LONG).show();
        updateScore();
        resetGame(p1Points, p2Points);
    }

    private void p2Wins(){
        p2Points++;
        Toast.makeText(this, "Player 2 wins!", Toast.LENGTH_LONG).show();
        updateScore();
        resetGame(p1Points, p2Points);
    }

    private void draw(){
        Toast.makeText(this, "It's a Draw!", Toast.LENGTH_LONG).show();
        resetGame(p1Points, p2Points);
    }

    private void updateScore(){
        textViewP1.setText("Player 1: " + p1Points);
        textViewP2.setText("Player 2: " + p2Points);
    }

    private void resetBoard(){
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                buttons[i][j].setText("");
            }
        }
        p1turn = true;
        roundCounts = 0;
    }

    private void resetGame(int x, int y){
        this.p1Points = x;
        this.p2Points = y;
        updateScore();
        enableGame(false);
        bReset.setText(R.string.play);
    }

    private void enableGame(boolean x){
        for(int i = 0; i < 3; i++){
            for(int j = 0; j < 3; j++){
                buttons[i][j].setClickable(x);
            }
        }
    }

    private boolean gameIsEnable(){
        return buttons[0][0].isClickable();
    }

    @Override
    public void onSaveInstanceState(Bundle outState, PersistableBundle outPersistentState) {
        super.onSaveInstanceState(outState, outPersistentState);

        outState.putInt("p1Points", p1Points);
        outState.putInt("p2Points", p2Points);
        outState.putInt("roundCounts", roundCounts);
        outState.putBoolean("p1turn", p1turn);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        p1Points = savedInstanceState.getInt("p1Points");
        p2Points = savedInstanceState.getInt("p2Points");
        roundCounts = savedInstanceState.getInt("roundCounts");
        p1turn = savedInstanceState.getBoolean("p1turn");
    }
}
